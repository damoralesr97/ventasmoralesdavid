// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBzFdfMBA8atDKvkEBBdGWrdCuW_fLXQXE',
    authDomain: 'ventasmoralesdavid.firebaseapp.com',
    databaseURL: 'https://ventasmoralesdavid.firebaseio.com',
    projectId: 'ventasmoralesdavid',
    storageBucket: 'ventasmoralesdavid.appspot.com',
    messagingSenderId: '205352179071',
    appId: '1:205352179071:web:4c0eec1d42bf3488c7f64a',
    measurementId: 'G-9BDLEPYJHN'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
