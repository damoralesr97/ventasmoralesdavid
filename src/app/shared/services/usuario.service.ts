import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Producto } from '../models/producto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  public carrito = [];

  constructor(private afAuth: AngularFireAuth, private angularFirestore: AngularFirestore) { }

  // RegistrarPaciente
  async onRegister(usuario: Usuario) {
    try{
      const pac = await this.afAuth.createUserWithEmailAndPassword(usuario.email, usuario.contrasena);
      usuario.uid = pac.user.uid;
      const param = JSON.parse(JSON.stringify(usuario));
      this.angularFirestore.collection('usuarios').doc(pac.user.uid).set(param);
      return pac;
    } catch (error) {
      console.log('Error on register user', error);
      return error;
    }
  }

  // Obtener productos
  async getProductos() {
    return this.angularFirestore.collection<Producto>('productos').snapshotChanges();
  }

  // Obtener producto
  getProducto(productoUid: string): Observable<any>{
    const itemDoc = this.angularFirestore.doc<any>(`productos/${productoUid}`);
    return itemDoc.valueChanges();
  }

}
