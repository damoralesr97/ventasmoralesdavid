export class Usuario {
    uid: string;
    cedula: string;
    nombres: string;
    apellidos: string;
    fechaNac: string;
    telefono: string;
    email: string;
    contrasena: string;
}
