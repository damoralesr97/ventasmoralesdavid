export class Producto {
    uid: string;
    nombre: string;
    precio: number;
    url: string;
}
