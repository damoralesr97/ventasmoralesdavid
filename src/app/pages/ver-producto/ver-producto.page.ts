import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../../shared/services/usuario.service';
import { Producto } from '../../shared/models/producto';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-ver-producto',
  templateUrl: './ver-producto.page.html',
  styleUrls: ['./ver-producto.page.scss'],
})
export class VerProductoPage implements OnInit {
  producto: Observable<any>;

  constructor(private route: ActivatedRoute, private usuarioSrv: UsuarioService, private alertController: AlertController) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.producto = this.usuarioSrv.getProducto(id);
    console.log(this.producto);
  }

  anadir(prodcuto){
    this.usuarioSrv.carrito.push(prodcuto);
    this.presentAlert('Producto agregado al carrito con éxito!');
  }

  // Mostrar alertas
  async presentAlert(mensaje: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Correcto',
      subHeader: mensaje,
      buttons: ['OK']
    });

    await alert.present();
  }

}
