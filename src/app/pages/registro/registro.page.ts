import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../shared/services/usuario.service';
import { Usuario } from '../../shared/models/usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  usuario: Usuario = new Usuario();

  constructor(private usuarioSrv: UsuarioService, private router: Router) { }

  ngOnInit() {
  }

  registrar(){
    this.usuarioSrv.onRegister(this.usuario);
    this.router.navigateByUrl('login');
  }

}
