import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { UsuarioService } from '../../shared/services/usuario.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  productos: any[] = [];

  constructor(private afAuth: AngularFireAuth, private router: Router, private userSrv: UsuarioService) { }

  async ngOnInit() {
    await (await this.userSrv.getProductos()).pipe(first()).toPromise().then( resp => {
      resp.forEach((producto) => {
        this.productos.push(producto.payload.doc.data());
      });
    });
    console.log(this.productos);
  }

  logout() {
    this.afAuth.signOut();
    this.router.navigateByUrl('login');
  }

  showProducto(productoUid: string) {
    this.router.navigate([`ver-producto/${productoUid}`]);
  }

}
