import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { UsuarioService } from '../../shared/services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-carrito-compras',
  templateUrl: './carrito-compras.page.html',
  styleUrls: ['./carrito-compras.page.scss'],
})
export class CarritoComprasPage implements OnInit {
  productos = [];
  total = 0;

  constructor(private callNumber: CallNumber, private usarioSrv: UsuarioService, private router: Router) { }

  ngOnInit() {
    this.productos = this.usarioSrv.carrito;
    for (let a of this.productos){
      this.total = this.total + a.precio;
    }
  }

  llamar() {
    this.callNumber.callNumber('+593998094612', true)
  .then(res => console.log('Llamando!', res))
  .catch(err => console.log('Error al realizar la llamada', err));
  }

  vaciarCarrito() {
    this.usarioSrv.carrito = [];
    this.router.navigateByUrl('home');
  }

}
